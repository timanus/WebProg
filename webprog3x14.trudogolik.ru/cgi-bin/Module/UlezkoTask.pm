package Module::UlezkoTask;
use strict;
use warnings FATAL => 'all';
require Lib::io_cgi;
require Model::Task;

sub new
{
  my $this = shift;
  my $self = {};
  bless $self, $this;
}

sub print_list
{
  my $io_cgi = Lib::io_cgi->new;
  my $group_id = $io_cgi->param("group_id");

  my $tasks = Model::Task->get_list($group_id);
  foreach my $task (@$tasks)
  {
    delete $task->{id};
  }

  print "Content-type: text/html;\n\n";

  require HTML::Template;
  my $template = HTML::Template->new( filename => "./templates/task_list.html" );
  $template->param(task => $tasks);
  print $template->output;
}
1;
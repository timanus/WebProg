package Module::UlezkoResult;
use strict;
use warnings FATAL => 'all';
require Lib::io_cgi;
require Model::Result;

sub new
{
  my $this = shift;
  my $self = {};
  bless $self, $this;
}

sub print_list
{
  my $io_cgi = Lib::io_cgi->new;
  my $group_id = $io_cgi->param("group_id");

  my $results = Model::Result->get_list($group_id);
  foreach my $result (@$results)
  {
    foreach my $result_info ( @{ $result->{results} } )
      {
        delete $result_info->{id};
      }
  }

  print "Content-type: text/html;\n\n";

  require HTML::Template;
  my $template = HTML::Template->new( filename => "./templates/result_list.html" );
  $template->param(task => $results);
  print $template->output;
}
1;
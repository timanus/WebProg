package Module::UlezkoGroup;
use strict;
use warnings FATAL => 'all';
require Lib::io_cgi;
require Model::Group;

sub new
{
  my $this = shift;
  my $self = {};
  bless $self, $this;
}

sub print_list
{
  my $groups = Model::Group->get_list;

  print "Content-type: text/html;\n\n";

  require HTML::Template;
  my $template = HTML::Template->new( filename => "./templates/group_list.html" );
  $template->param(group_info => $groups);
  print $template->output;
}

sub add {
  my $this = shift;

  my $io_cgi = Lib::io_cgi->new;

  my $title = $io_cgi->param("title");
  my $chat_id = $io_cgi->param("chat_id");

  require Entity::Group;
  Entity::Group->add($title, $chat_id);

  $this->print_list;
}

sub delete
{
  my $this = shift;

  my $io_cgi = Lib::io_cgi->new;

  my $id = $io_cgi->param("id");

  require Model::Group;
  Model::Group->delete($id);

  $this->print_list;
}
1;
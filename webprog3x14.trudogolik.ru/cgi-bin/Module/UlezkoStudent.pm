package Module::UlezkoStudent;
use strict;
use warnings FATAL => 'all';
require Lib::io_cgi;
require Model::Student;

sub new
{
  my $this = shift;
  my $self = {};
  bless $self, $this;
}

sub print_list
{
  my $io_cgi = Lib::io_cgi->new;
  my $group_id = $io_cgi->param("group_id");

  my $students = Model::Student->get_list($group_id);
  foreach my $student (@$students)
  {
    delete $student->{id};
  }

  print "Content-type: text/html;\n\n";

  require HTML::Template;
  my $template = HTML::Template->new( filename => "./templates/student_list.html" );
  $template->param(student => $students);
  print $template->output;
}
1;
package Model::Result;
use strict;
use warnings FATAL => 'all';
require Lib::Database;
my $dbh = Lib::Database->new;

sub get_list
{
  my ($this, $group_id) = @_;

  my @result;
  my $sth = $dbh->prepare("SELECT id,number FROM tgbot_task WHERE group_id = ?");
  $sth->execute($group_id);

  while (my $row = $sth->fetchrow_hashref)
  {
    my $task = {};
    $task->{number} = $row->{number};
    $task->{results} = _get_results_by_task_id($row->{id});

    push @result, $task;
  }

  return \@result;
}

# ��������� ������ id �������, ���������� ������ ���� ����������� �� ���
sub get_list_by_task_list
{
  my ($this, $task_id_list) = @_;

  my @result;
  foreach my $task_id (@{$task_id_list})
  {
    my @results = @{ _get_results_by_task_id($task_id) };
    foreach my $row (@results)
    {
      push @result, $row;
    }
  }

  return \@result;
}
################################ FOR INTERNAL USE ##################################

sub _get_student
{
  my $student_id = pop;
  my $sth = $dbh->prepare("SELECT name FROM tgbot_student WHERE id = ?");
  $sth->execute($student_id);

  return $sth->fetchrow_arrayref->[0];
}

sub _get_results_by_task_id
{
  my $task_id = shift;
  my $result = [];
  my $sth = $dbh->prepare("SELECT id,student_id,mark FROM tgbot_result WHERE task_id = ?");
  $sth->execute($task_id);

  while (my $row = $sth->fetchrow_hashref)
  {
    $row->{name} = _get_student($row->{student_id});
    delete $row->{student_id};
    push @$result, $row;
  }

  return $result;
}
1;
package Model::Student;
use strict;
use warnings FATAL => 'all';
require Lib::Database;

sub get_list
{
  my ($this, $group_id) = @_;

  my $dbh = Lib::Database->new;

  my $sth = $dbh->prepare("SELECT id,name FROM tgbot_student WHERE group_id=? ORDER BY name");
  $sth->execute($group_id);

  my @result;

  while (my $row = $sth->fetchrow_hashref)
  {
    push @result, $row;
  }

  return \@result;
}

1;
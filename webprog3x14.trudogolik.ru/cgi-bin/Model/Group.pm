package Model::Group;
use strict;
use warnings FATAL => 'all';
require Lib::Database;
my $dbh = Lib::Database->new;

sub get_list
{
  my $sth = $dbh->prepare("SELECT id,title,amount_of_students,average_mark FROM tgbot_group");
  $sth->execute;

  my @groups_info;

  while ( my $row = $sth->fetchrow_hashref )
  {
    if (! defined $row->{average_mark}) {
      $row->{average_mark} = "-";
    }
    push @groups_info, $row;
  }

  return \@groups_info;
}

sub delete
{
  require Model::Task;
  require Model::Result;
  require Model::Student;
  require Entity::Task;
  require Entity::Student;
  require Entity::Result;

  my ($this, $id) = @_;

  my $task_list = Model::Task->get_list($id);
  my $student_list = Model::Student->get_list($id);

  my @task_id_list;
  foreach my $task (@{$task_list})
  {
    push @task_id_list, $task->{id};
  }

  my $result_list = Model::Result->get_list_by_task_list(\@task_id_list);

  foreach my $task (@task_id_list)
  {
    Entity::Task->delete($task);
  }
  foreach my $student (@$student_list)
  {
    Entity::Student->delete($student->{id});
  }
  foreach my $result (@$result_list)
  {
    Entity::Result->delete($result->{id});
  }

  my $sth = $dbh->prepare("DELETE FROM tgbot_group WHERE id=?");
  $sth->execute($id);
}
1;
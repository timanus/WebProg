package Model::Task;
use strict;
use warnings FATAL => 'all';
require Lib::Database;
my $dbh = Lib::Database->new;

sub get_list
{
  my ($this, $group_id) = @_;

  my $sth = $dbh->prepare("SELECT id,number,deadline,text FROM tgbot_task WHERE group_id=? ORDER BY number");
  $sth->execute($group_id);

  my @result;

  while (my $row = $sth->fetchrow_hashref)
  {
    push @result, $row;
  }

  return \@result;
}

sub get_list_by_group_id
{
  my ($this, $group_id) = @_;

  my $sth = $dbh->prepare("SELECT id FROM tgbot_task WHERE group_id=? ORDER BY number");
  $sth->execute($group_id);

  my @result;
  while ( my @row = $sth->fetchrow_arrayref)
  {
    push @result, shift(@row);
  }

  return \@result;
}
1;
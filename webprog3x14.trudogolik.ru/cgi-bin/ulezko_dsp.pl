#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use Data::Dumper;
use lib ".";

eval
{
  # Подключаем CGI и базу данных
  require Lib::io_cgi;
  require Lib::Database;

  # Открываем подключение к бд
  Lib::Database->new;

  # Инициализация параметров CGI
  my $io_cgi = Lib::io_cgi->new;

  $io_cgi->get_params();
  my $class = $io_cgi->param("class");
  my $event = $io_cgi->param("event");

  # Формируем строку для подключения и использования модуля
  my $str = "Module::" . $class;

  eval "require $str";
  my $object = $str->new;
  $object->$event();
};

if ($@)
{
  print "Content-type: text/html\n\n";
  print $@;
}






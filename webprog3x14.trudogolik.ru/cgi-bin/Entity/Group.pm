package Entity::Group;
use strict;
use warnings FATAL => 'all';
require Lib::Database;
my $dbh = Lib::Database->new;

sub add
{
  my ($this, $title, $chat_id) = @_;

  my $sth = $dbh->prepare("INSERT tgbot_group (title,chat_id) VALUES (?,?)");
  $sth->execute($title, $chat_id);
}

1;
package Entity::Result;
use strict;
use warnings FATAL => 'all';
require Lib::Database;
my $dbh = Lib::Database->new;

sub delete
{
  my ($this, $id) = @_;

  my $sth = $dbh->prepare("DELETE FROM tgbot_result WHERE id=?");
  $sth->execute($id);
}
1;
#!/usr/bin/perl
package Lib::Database;
use strict;
use warnings FATAL => 'all';
use DBI;

our $singleton = undef;

sub new {
  my $class = shift;

  return $singleton if defined $singleton;

  my $attr = {PrintError => 0, RaiseError => 0};
  my $data_source = "DBI:mysql:webprog3x14_tgbot:localhost";
  my $username = "webprog3x14_tgbot";
  my $password = "Aboba1243";
  my $dbh = DBI->connect($data_source, $username, $password, $attr);
  if (!$dbh) { die $DBI::errstr; }
  $dbh->do("SET NAMES cp1251");

  $singleton = $dbh;

  return $singleton;
}
1;
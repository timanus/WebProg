#!/usr/bin/perl -w
use strict;
use warnings FATAL => 'all';
use lib ".";
require Entity::Group;
require Model::Task;
require Model::Student;
require Model::Result;
require Model::Group;
use Data::Dumper;

my $results = Model::Result->get_list(1);
foreach my $result (@$results)
{
  delete $result->{id};
}
print Dumper($results);

